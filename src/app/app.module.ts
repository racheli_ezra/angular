import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MegaMenuModule } from 'primeng/megamenu';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import { CreateMinyanComponent } from './components/create-minyan/create-minyan.component';
import { HttpClientModule } from '@angular/common/http';
import { EventService } from './services/event.service';
import { NusachService } from './services/nusach.service';
import { PrayService } from './services/pray.service';
import { ToraService } from './services/tora.service';
import { FormsModule,ReactiveFormsModule } from "@angular/forms";
import { MinyanDetailesComponent } from './components/minyan-detailes/minyan-detailes.component';
import { RishumMinyanComponent } from './components/rishum-minyan/rishum-minyan.component';
import { ListboxModule } from 'primeng/listbox';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccordionModule } from 'primeng/accordion';
import { RouterModule } from '@angular/router';
import { InputTextModule } from 'primeng/inputtext';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { DropdownModule } from 'primeng/dropdown';
import {CheckboxModule} from 'primeng/checkbox';
import {MinynimZminimComponent} from './components/minynim-zminim/minynim-zminim.component';
// import {ListboxModule} from 'primeng/listbox';
import {InputNumberModule} from 'primeng/inputnumber';
import { AgmCoreModule, AgmRectangle } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import {RadioButtonModule} from 'primeng/radiobutton';
import {TableModule} from 'primeng/table';
import { MyMinynimComponent } from './components/my-minynim/my-minynim.component';
import {CalendarModule} from 'primeng/calendar';
import { UserComponent } from './components/user/user.component';
// import { AddUserComponent } from './components/add-user/add-user.component';
import {PanelModule} from 'primeng/panel'
import {DialogModule} from 'primeng/dialog';
import {SpinnerModule} from 'primeng/spinner';
import {ButtonModule} from 'primeng/button';
import {PasswordModule} from 'primeng/password';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {TooltipModule} from 'primeng/tooltip';
import {MultiSelectModule} from 'primeng/multiselect';

// import {MessageModule} from 'primeng/message';
import {NgbPaginationModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';
import {CaptchaModule} from 'primeng/captcha';
import { FooterComponent } from './components/footer/footer.component';
import { ConfirmFormComponent } from './components/confirm-form/confirm-form.component';
import { HomeComponent } from './components/home/home.component';

import { ForgetPasswordComponent } from './components/user/forget-password/forget-password.component';
import { ResetPasswordComponent } from './components/user/reset-password/reset-password.component';
import { RegisterUserComponent } from './components/user/register-user/register-user.component';
import { GalleriaComponent } from './components/galleria/galleria.component';
import {GalleriaModule} from 'primeng/galleria';
import {MenuModule} from 'primeng/menu';



@NgModule({
  declarations: [
    AppComponent,
    CreateMinyanComponent,
    MinyanDetailesComponent,
    RishumMinyanComponent,
    MinynimZminimComponent,
    MyMinynimComponent,
    UserComponent,
    // AddUserComponent,
    FooterComponent,
    ConfirmFormComponent,
    HomeComponent,
    ResetPasswordComponent,
    ForgetPasswordComponent,
    RegisterUserComponent,
    GalleriaComponent
  ],
  imports: [
    MultiSelectModule,
    BrowserModule, BrowserAnimationsModule,
    RouterModule,PanelModule,
    AppRoutingModule,
    HttpClientModule,
    MenuModule,
    FormsModule,ReactiveFormsModule,
     CalendarModule,
    ListboxModule, AccordionModule,
    MegaMenuModule,
    NgbModule,
    NgbAlertModule,
    InputTextModule,
    MessagesModule,
    MessageModule,
    DropdownModule,
    InputNumberModule,
    GooglePlaceModule,
    RadioButtonModule,
    NgbPaginationModule,
    CheckboxModule,
    TableModule,
    CalendarModule,
    DialogModule,
    SpinnerModule,
    ButtonModule,
    PasswordModule,
    CaptchaModule,
    TooltipModule,
    GalleriaModule,

    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAj13gY0dRy3LKgxfbOkCPaqq_twe8eR3k',
      libraries:['places','geometry','drawing']
    }),
  ],
  
  providers: [EventService, NusachService, PrayService, ToraService],
  bootstrap: [AppComponent]
})
export class AppModule { }
