import { TestBed } from '@angular/core/testing';

import { MyMinyanService } from './my-minyan.service';

describe('MyMinyanService', () => {
  let service: MyMinyanService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MyMinyanService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
