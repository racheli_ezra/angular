import { Injectable } from '@angular/core';
import { User } from '../classes/user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Identifiers } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  ListU: Array<User> = new Array<User>();
  user: User = new User();

  url: string = "http://localhost:56105/api/User/";

  constructor(public http: HttpClient) { }

  ngOnInit() {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
  

  }

  getUser(id: string, password: string): Observable<User> {
      debugger
    return this.http.get<User>(this.url + "GetUser/" + id + "/" + password);

  }
  add(u: User): Observable<void> {
    debugger
    return this.http.put<void>(this.url + "Add/", u)
  }
  sendNewPassword(obj:any){
    debugger
    return this.http.post<any>(this.url +  "SendNewPassword/" ,obj);
  }
  GetAllUsersMail(): Observable<Array<String>> {
    return this.http.get<Array<String>>(this.url + "GetAllUsersMail");
  }

  // לשאול את אסתי שטראוס
  // recognizeUser(Mail: string, Password: string) {
  //   this.user.id = Mail;
  //   this.user.password = Password;
  //  // להוסיף פונקציה של שליחת סיסמא חדשה
  //   // בסרבר
  //   return this.http.post<any>(this.url + "PostEmployeeByPassword", this.user);
  // }

  changePassword(mail: string, newPassword1: string) {
    debugger
    // var u=new User();
    // u.Id=Mail;
    // u.password=newPassword1;
    return this.http.post<void>(this.url + "ChangePassword" , mail + "/" + newPassword1);
  }
}
