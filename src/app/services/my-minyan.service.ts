import { Injectable } from '@angular/core';
import { Minyan } from '../classes/minyan';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MyMinyanService {
  ListMM:Array<Minyan>=new Array<Minyan>();

  url:string="http://localhost:56105/api/Minyan/"
    constructor(private http:HttpClient) { }
    GetAllMinyanimByUserId(mail:string):Observable<Array<Minyan>>{
      debugger
      return this.http.get<Array<Minyan>>(this.url + "GetAllMinyanimByUserId/"+ mail+"/")
    }
}
