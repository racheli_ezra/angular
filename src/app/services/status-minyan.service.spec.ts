import { TestBed } from '@angular/core/testing';

import { StatusMinyanService } from './status-minyan.service';

describe('StatusMinyanService', () => {
  let service: StatusMinyanService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StatusMinyanService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
