import { TestBed } from '@angular/core/testing';

import { MinynimZminimService } from './minynim-zminim.service';

describe('MinynimZminimService', () => {
  let service: MinynimZminimService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MinynimZminimService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
