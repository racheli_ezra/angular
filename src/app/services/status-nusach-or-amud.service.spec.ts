import { TestBed } from '@angular/core/testing';

import { StatusNusachOrAmudService } from './status-nusach-or-amud.service';

describe('StatusNusachOrAmudService', () => {
  let service: StatusNusachOrAmudService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StatusNusachOrAmudService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
