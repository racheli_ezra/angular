import { TestBed } from '@angular/core/testing';

import { TimeMinyanService } from './time-minyan.service';

describe('TimeMinyanService', () => {
  let service: TimeMinyanService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TimeMinyanService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
