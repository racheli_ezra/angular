import { TestBed } from '@angular/core/testing';

import { NusachService } from './nusach.service';

describe('NusachService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NusachService = TestBed.get(NusachService);
    expect(service).toBeTruthy();
  });
});
