import { TestBed } from '@angular/core/testing';

import { MinyanService } from './minyan.service';

describe('MinyanService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MinyanService = TestBed.get(MinyanService);
    expect(service).toBeTruthy();
  });
});
