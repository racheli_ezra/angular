import { Injectable } from '@angular/core';
import { ResonForAmud } from '../classes/reson-for-amud';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  ListRE: Array<ResonForAmud> = new Array<ResonForAmud>();
  url: string = "http://localhost:56105/api/ResonForEvent/";
  constructor(private http: HttpClient) { }

  GetAll(): Observable<Array<ResonForAmud>> {
    return this.http.get<Array<ResonForAmud>>(this.url + "GetAll")
  }
  GetIdByEvent(reson:string):Observable<any>
  {
    debugger
     return this.http.get<any>(this.url + "GetIdByEvent/" +reson)
  }
}