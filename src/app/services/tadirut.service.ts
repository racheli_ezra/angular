import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tadirut } from '../classes/tadirut';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TadirutService {
ListT:Tadirut[]=[];
  constructor(private http:HttpClient) { }
  url: string = "http://localhost:56105/api/Tadirut/";
  GetAll():Observable<Array<Tadirut>>{
    return this.http.get<Array<Tadirut>>(this.url+"GetAll");
  }

}
