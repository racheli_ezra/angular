import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StatusNusachOrAmud } from '../classes/status-nusach-or-amud';

@Injectable({
  providedIn: 'root'
})
export class StatusNusachOrAmudService {

  listS:Array<StatusNusachOrAmud> = new Array<StatusNusachOrAmud>();
  url:string = "http://localhost:56105/api/StatusNusachOrAmud/";

  constructor(private http:HttpClient) { }

  GetAll():Observable<Array<StatusNusachOrAmud>>{
    debugger;
    return this.http.get<Array<StatusNusachOrAmud>>(this.url +"GetAll");

  }
}

// http://localhost:56105/api/StatusNusachOrAmud/GetAll