import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DateConversionService {

  s: string;
  constructor(private http: HttpClient) { }
  getHebrew(da: Date): Observable<any> {

    let y: number = new Date(da).getFullYear();
    let m: number = new Date(da).getMonth() + 1
    let d: number = new Date(da).getDate();

    this.s = "https://www.hebcal.com/converter/?cfg=json&gy=" + y + "&gm=" + m + "&gd=" + d + "&g2h=1";
    //   https://www.hebcal.com/converter?cfg=json&gy=2011&gm=6&gd=2&g2h=1

    return this.http.get<any>(this.s);
  }
}
