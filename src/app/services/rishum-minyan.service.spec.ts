import { TestBed } from '@angular/core/testing';

import { RishumMinyanService } from './rishum-minyan.service';

describe('RishumMinyanService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RishumMinyanService = TestBed.get(RishumMinyanService);
    expect(service).toBeTruthy();
  });
});
