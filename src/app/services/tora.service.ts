import { Injectable } from '@angular/core';
import { Torah } from '../classes/torah';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ToraService {

  ListT: Array<Torah> = new Array<Torah>();
  url: string = "http://localhost:56105/api/Tora/";
  constructor(private http: HttpClient) { }

  GetAll(): Observable<Array<Torah>> {
    return this.http.get<Array<Torah>>(this.url + "GetAll")
  }
  GetIdByTora(nusach: string): Observable<any> {
    return this.http.get<any>(this.url + "GetIdByNusach/" + nusach)
  }
}


