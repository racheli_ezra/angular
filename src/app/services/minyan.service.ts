import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Minyan } from '../classes/minyan';
import { HttpClient } from '@angular/common/http';
import { TimesMinyan } from '../classes/times-minyan';

@Injectable({
  providedIn: 'root'
})
export class MinyanService {
  MList: Array<Minyan> = new Array<Minyan>();
  ListFoundPlace: Array<Minyan> = new Array<Minyan>();
  url: string = "http://localhost:56105/api/Minyan/";
  constructor(private http: HttpClient) { }
  Add(MinyanDinmic: Array<any>): Observable<Minyan> {
    debugger;
    console.log(MinyanDinmic[0])
    return this.http.put<Minyan>(this.url + "Add/", MinyanDinmic)
  }
  getAll(): Observable<Array<Minyan>> {
    debugger
    return this.http.get<Array<Minyan>>(this.url + "GetAll")
  }
  getMinyanById(id: number): Observable<Minyan> {
    debugger
    return this.http.get<Minyan>(this.url + "GetMinyanById/" + id)
  }
  //  getCarsSmall() {
  //         return this.http.get<any>(this.url+"GetAll")
  //         .toPromise()
  //         .then(res => <Minyan[]>res.data)
  //         .then(data => { return data; });
  //     }
}
