import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RishumMinyan } from '../classes/rishum-minyan';

@Injectable({
  providedIn: 'root'
})
export class RishumMinyanService {

  url: string = "http://localhost:56105/api/RishumMinyan/";
  constructor(private http: HttpClient) { }
  id:number
  isManager:number
  idNusach: any
  idEvent: any
  idTora: any
  rishum(r: RishumMinyan): Observable<void> {

    //  r.KadishId  = 1
    debugger
    return this.http.post<void>(this.url + "Add", r);
  }

  cancalRishum(minyanId: number, mail: string): Observable<boolean> {
    debugger
    return this.http.delete<boolean>(this.url + 'CancelRishum/' + minyanId + "/" + mail+"/")

  }
}
