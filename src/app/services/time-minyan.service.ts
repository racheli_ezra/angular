import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TimesMinyan } from '../classes/times-minyan';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimeMinyanService {
  url: string = "http://localhost:56105/api/TimeMinyan/";
  constructor(private http: HttpClient) { }
  add(a: Array<TimesMinyan>): Observable<void> {
    debugger;
    return this.http.put<void>(this.url + "Add", a);
  }
  getAll(): Observable<Array<TimesMinyan>> {
    debugger;
    return this.http.get<Array<TimesMinyan>>(this.url + "GetAll");
  }
}
