import { Injectable } from '@angular/core';
import { Pray } from '../classes/pray';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PrayService {
ListP:Array<Pray>=new Array<Pray>();
url: string = "http://localhost:56105/api/Pray/";
constructor(private http: HttpClient) { }

GetAll(): Observable<Array<Pray>> {
  // debugger
  return this.http.get<Array<Pray>>(this.url + "GetAll")
}}
