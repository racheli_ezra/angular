import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StatusMinyanService {
  url: string = "http://localhost:56105/api/StatusMinyan/";
  constructor(private http: HttpClient) { }
  GetIdByStarusMinyan(status: string): Observable<number> {
    return this.http.get<number>(this.url + "getIdByStatusMinyan/" + status)
  }
  GetStatusMinyanById(StatusId: number): Observable<string> {
    return this.http.get<string>(this.url + "getStarusMinyanById/" + StatusId)
  }
}
