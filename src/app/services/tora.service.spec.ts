import { TestBed } from '@angular/core/testing';

import { ToraService } from './tora.service';

describe('ToraService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ToraService = TestBed.get(ToraService);
    expect(service).toBeTruthy();
  });
});
