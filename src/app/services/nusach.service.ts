import { Injectable } from '@angular/core';
import { Nusach } from '../classes/nusach';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class NusachService {

  ListN: Array<Nusach> = new Array<Nusach>();
  url: string = "http://localhost:56105/api/Nusach/";
  constructor(private http: HttpClient) { }

  GetAll(): Observable<Array<Nusach>> {
    return this.http.get<Array<Nusach>>(this.url + "GetAll")
  }
  GetIdByNusach(nusach:string):Observable<any>
  {
    debugger
     return this.http.get<any>(this.url + "GetIdByNusach/" + nusach)
  }
}
