import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RishumMinyanComponent } from './components/rishum-minyan/rishum-minyan.component';
import { MinyanDetailesComponent } from './components/minyan-detailes/minyan-detailes.component';
import { createComponent } from '@angular/compiler/src/core';
import { CreateMinyanComponent } from './components/create-minyan/create-minyan.component';
import { MinynimZminimComponent } from './components/minynim-zminim/minynim-zminim.component';
import { MyMinynimComponent } from './components/my-minynim/my-minynim.component';
import { UserComponent } from './components/user/user.component';
// import { AddUserComponent } from './components/add-user/add-user.component';
import { AppComponent } from './components/app/app.component';
import { ConfirmFormComponent } from './components/confirm-form/confirm-form.component';
import { HomeComponent } from './components/home/home.component';
import { ForgetPasswordComponent } from './components/user/forget-password/forget-password.component';
import { ResetPasswordComponent } from './components/user/reset-password/reset-password.component';
import { RegisterUserComponent } from './components/user/register-user/register-user.component';


const routes: Routes = [
  {
    path: 'forgetPassword',
    component: ForgetPasswordComponent
  },
  {
    path: 'resetPassword',
    component: ResetPasswordComponent
  },
  {
    path: 'registerUser',
    component: RegisterUserComponent
  },
  { 
    path: "app", 
    component: AppComponent 
  },
  { 
    path: "home", 
    component: HomeComponent 
  },
  {
    path: "MinyanDetailes", 
    component: MinyanDetailesComponent }
  , { path: "RishumMinyan/:id/:isManager", component: RishumMinyanComponent }
  , { path: "CreateMinyan", component: CreateMinyanComponent }
  , { path: "MinyaninZminim", component: MinynimZminimComponent }
  , { path: "User/:nituv", component: UserComponent }
  // , { path: "AddUser/:id/:password", component: AddUserComponent }
  , { path: "MyMinyan", component: MyMinynimComponent }
  , { path: "ConfirmForm", component: ConfirmFormComponent },
]


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
