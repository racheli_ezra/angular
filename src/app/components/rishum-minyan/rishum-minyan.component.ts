import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MinyanService } from 'src/app/services/minyan.service';
import { Minyan } from 'src/app/classes/minyan';
import { Nusach } from 'src/app/classes/nusach';
import { NusachService } from 'src/app/services/nusach.service';
import { RishumMinyan } from 'src/app/classes/rishum-minyan';
import { SelectItem } from 'primeng/api';
import { PrayService } from 'src/app/services/pray.service';
import { EventService } from 'src/app/services/event.service';
import { ToraService } from 'src/app/services/tora.service';
import { RishumMinyanService } from 'src/app/services/rishum-minyan.service';
import { StatusNusachOrAmudService } from 'src/app/services/status-nusach-or-amud.service';
import { Time } from '@angular/common';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { data } from 'jquery';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/classes/user';
import { ResonForAmud } from 'src/app/classes/reson-for-amud';
import { Torah } from 'src/app/classes/torah';
import { DateConversionService } from 'src/app/services/date-conversion.service';
// import { time } from 'console';
@Component({
  selector: 'app-rishum-minyan',
  templateUrl: './rishum-minyan.component.html',
  styleUrls: ['./rishum-minyan.component.css'],
  providers: [MessageService],
  "styles": [
    "node_modules/primeflex/primeflex.css"
  ],

})
export class RishumMinyanComponent implements OnInit {
  // id: number;
  RishumSuccessfully: boolean = false;
  RishumNotSuccessfully: boolean = false;

  city1: any = null;
  city2: any = null;
  // first: number;
  minyan: Minyan = new Minyan();
  rishum: RishumMinyan = new RishumMinyan();
  Nusach: Nusach = new Nusach();
  NusachList: SelectItem[];
  succ: boolean
  displayBasic: boolean = false;

  // primeNGיצרנו את 3 המשתנים האלה בגלל שבחירה ב 
  //id נבחר אובקייט שלם ואנחנו צריכים רק 
  idNusach: Nusach = new Nusach()
  idAmud: ResonForAmud = new ResonForAmud()
  idTora: Torah = new Torah()
  rishumMinyanForm: FormGroup;
  flag1: boolean = false;
  flag2: boolean = false;
  flag3: boolean = false;
  gmishutBeforeHour: any;
  gmishutAfterHour: any;
  gmishutBeforeMinute: any;
  gmishutAfterMinute: any;
  flag: boolean = true;
  userLogin1: boolean = false;

  EMAIL_REGEX = '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$'

  minDate: Date;
  maxDate: Date;

  constructor(
    private active: ActivatedRoute,
    private route: Router,
    private MinyanService: MinyanService,
    public NusachService: NusachService,
    public EventService: EventService,
    public ToraService: ToraService,
    public RishumMinyanService: RishumMinyanService,
    public UserService: UserService,
    public StatusNusachOrAmudService: StatusNusachOrAmudService,
    public DateConversionService: DateConversionService,
    public fb: FormBuilder, public messageService: MessageService,
  ) {
  }
  getMinyanById(id: number) {
    this.MinyanService.getMinyanById(id).subscribe(data => {
      this.minyan = data;
      this.minyan.Datetime = new Date(this.minyan.Datetime)
      this.minyan.Hour = new Date(this.minyan.Hour)
      this.rishum.Hour = this.minyan.Hour;
      this.rishum.Mail = this.UserService.user.id;
      this.rishum.Name = this.UserService.user.firstName + " " + this.UserService.user.lastName;

      if (this.RishumMinyanService.isManager == 0) {
        this.rishum.Mail = this.UserService.user.id;
        this.rishum.IsManger = true;


        this.NusachService.GetIdByNusach(this.minyan.Nusach).subscribe(data => { this.idNusach = this.NusachService.ListN.find(s => s.Id == data.Id) }, err => { })
        this.ToraService.GetIdByTora(this.minyan.Tora).subscribe(data => { this.idTora = this.ToraService.ListT.find(t => t.Id == data.Id) });
        this.EventService.GetIdByEvent(this.minyan.Amud).subscribe(data => { this.idAmud = this.EventService.ListRE.find(t => t.Id == data.Id) });
      }

      if (this.minyan.Tadirut == "חד פעמי") {
        debugger;
        this.DateConversionService.getHebrew(this.minyan.Datetime).subscribe(data => { this.minyan.heDate = data.hebrew })
      }

      // this.minyan.Hour = { hours: d.getHours(), minutes: d.getMinutes() }
      this.addMinutes();
    }, err => { debugger; err })
  }

  ngOnInit() {
    debugger
    this.active.params.subscribe(f => {
      debugger
      this.RishumMinyanService.id = f["id"] as number;
      this.RishumMinyanService.isManager = f["isManager"] as number;

    })

    this.getMinyanById(this.RishumMinyanService.id);

    //הנתונים של המניין לא נטענים טוב
    this.getAllNusach();
    this.getAllEvent();
    this.getAllTora();
    this.getAllStatusNusachOrAmud();
    this.userLogin();
    debugger;
    // this.rishum.Hour = this.minyan.Hour;
    // this.rishum.Mail = this.UserService.user.id;
    // this.rishum.Name = this.UserService.user.firstName + this.UserService.user.lastName;

    //יוצר המניין
    //כל נרשם אחר למניין
    if (this.RishumMinyanService.isManager == 1) {
      debugger
      this.NusachService.GetIdByNusach("לא משנה").subscribe(data => {debugger; this.idNusach = this.NusachService.ListN.find(s => s.Id == data.Id) }, err => { })
      this.ToraService.GetIdByTora("לא משנה").subscribe(data => { this.idTora = this.ToraService.ListT.find(t => t.Id == data.Id) });
      debugger;
      this.EventService.GetIdByEvent("אין צורך").subscribe(data => { this.idAmud = this.EventService.ListRE.find(t => t.Id == data.Id) });
    }

    // if(this.minyan.Before != 0 )
    //     this.gmishutBefore = this.minyan.Hour -  (this.minyan.Before)
    //   var newDateObj = moment(oldDateObj).add(30, 'm').toDate();


  }
  userLogin() {
    debugger;
    if (this.UserService.user.id == null) {
      this.userLogin1 = false;
    }
    else {
      this.userLogin1 = true;
    }


  }
  User() {
    this.route.navigate(["User/", 3])
  }

  addMinutes() {
    var date1 = new Date(this.minyan.Hour)
    var date2 = new Date(this.minyan.Hour)
    // this.minDate = new Date(this.minyan.Hour)
    // this.maxDate = new Date(this.minyan.Hour)
    // if (this.minyan.Before > 0 && this.minyan.After > 0) {
    this.minDate = new Date(date1.setTime(date1.getTime() - this.minyan.Before * 60 * 1000));
    this.maxDate = new Date(date2.setTime(date2.getTime() + this.minyan.After * 60 * 1000));
    // }

    debugger;
    this.gmishutAfterHour = this.minyan.Hour.getHours();
    this.gmishutAfterMinute = this.minyan.Hour.getMinutes();
    this.gmishutBeforeHour = this.minyan.Hour.getHours();
    this.gmishutBeforeMinute = this.minyan.Hour.getMinutes();

    this.gmishutBeforeHour -= Math.floor(this.minyan.Before / 60);
    this.gmishutBeforeMinute -= this.minyan.Before % 60;
    this.gmishutAfterHour += (this.minyan.After % 60);
    this.gmishutAfterMinute += Math.floor(this.minyan.After / 60);
    debugger
    // alert(this.gmishutAfterHour + " " + this.gmishutAfterMinute);
    if (this.gmishutAfterHour == NaN && this.gmishutAfterMinute == NaN)
      this.flag = false;
  }

  getAllNusach() {

    this.NusachService.GetAll().subscribe(data => this.NusachService.ListN = data)
  }

  getAllEvent() {

    this.EventService.GetAll().subscribe(data => { this.EventService.ListRE = data }, err => { })
  }

  getAllTora() {
    this.ToraService.GetAll().subscribe(data => { this.ToraService.ListT = data }, err => { })
  }

  getAllStatusNusachOrAmud() {
    debugger
    this.StatusNusachOrAmudService.GetAll().subscribe(data => {
      this.StatusNusachOrAmudService.listS = data;
      debugger
      this.rishum.StatusAmud = this.StatusNusachOrAmudService.listS.find(a => a.Status == ("חובה")).Id;
      this.rishum.StatusNusach = this.StatusNusachOrAmudService.listS.find(a => a.Status == ("חובה")).Id;
      debugger
      this.rishum.statusTora = this.StatusNusachOrAmudService.listS.find(a => a.Status == ("חובה")).Id;
    }, err => { })
  }

  Rishum() {
    debugger

    this.rishum.MinyanId = this.RishumMinyanService.id;
    // if (this.idNusach.Id)
    this.rishum.NusachId = this.idNusach.Id
    // else
    //   this.rishum.NusachId = null;
    this.rishum.torahId = this.idTora.Id
    this.rishum.ReasonAmudId = this.idAmud.Id

    this.RishumMinyanService.rishum(this.rishum).subscribe(data => { this.RishumSuccessfully = true }, err => { this.RishumNotSuccessfully = true })
    // {data==true?this.succ = true : this.succ=false;}
  }
  selectedNusach() {
    if (this.idNusach.Nusach1 == "לא משנה")
      this.flag1 = false;
    else
      this.flag1 = true;
  }
  selectedAmud() {
    if (this.idAmud.Reson == "אין צורך")
      this.flag2 = false;
    else
      this.flag2 = true;
  }
  selectedTora() {
    if (this.idTora.Nusach == "לא משנה")
      this.flag3 = false;
    else
      this.flag3 = true;
  }

  // this.NusachList = [
  //   { label: 'בחר נוסח', value: null },
  //   { label: 'New York', value: { id: 1, name: 'New York', code: 'NY' } },
  //   { label: 'Rome', value: { id: 2, name: 'Rome', code: 'RM' } },
  //   { label: 'London', value: { id: 3, name: 'London', code: 'LDN' } },
  //   { label: 'Istanbul', value: { id: 4, name: 'Istanbul', code: 'IST' } },
  //   { label: 'Paris', value: { id: 5, name: 'Paris', code: 'PRS' } },
  //   { label: this.NusachService.ListN[0].Nusach1, value: { id: this.NusachService.ListN[0].Id, name: this.NusachService.ListN[0].Nusach1, code: this.NusachService.ListN[0].Id } },
  // for(let i = 0 ; i < this.NusachService.ListN.length; i++){
  //   {label:this.NusachService.ListN[i].Nusach1, value:{id:this.NusachService.ListN[i].Id, name: this.NusachService.ListN[i].Nusach1, code: this.NusachService.ListN[i].Id}},
  // ]
}




