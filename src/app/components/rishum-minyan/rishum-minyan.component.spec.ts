import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RishumMinyanComponent } from './rishum-minyan.component';

describe('RishumMinyanComponent', () => {
  let component: RishumMinyanComponent;
  let fixture: ComponentFixture<RishumMinyanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RishumMinyanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RishumMinyanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
