import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { RishumMinyanService } from 'src/app/services/rishum-minyan.service';
import { MinynimZminimService } from 'src/app/services/minynim-zminim.service';
import { LazyLoadEvent } from 'primeng/api';
import { RishumMinyan } from 'src/app/classes/rishum-minyan';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { PrayService } from 'src/app/services/pray.service'
import { NusachService } from 'src/app/services/nusach.service'
import { MinyanService } from 'src/app/services/minyan.service'
import { Minyan } from 'src/app/classes/minyan';
import { TadirutService } from 'src/app/services/tadirut.service'
import { TimesMinyan } from 'src/app/classes/times-minyan';
import { TimeMinyanService } from 'src/app/services/time-minyan.service';
import { DateConversionService } from 'src/app/services/date-conversion.service';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { NgbCalendarHebrew, NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { Days } from 'src/app/classes/days';
// import { google } from '@agm/core/services/google-maps-types';
declare const google: any;

@Component({
  selector: 'app-minynim-zminim',
  templateUrl: './minynim-zminim.component.html',
  styleUrls: ['./minynim-zminim.component.css']
})
export class MinynimZminimComponent implements OnInit {
  [x: string]: any;
  @ViewChild("gmap") googleMap: any;
  @ViewChild("placesRef") placesRef: GooglePlaceDirective;
  loading: boolean;
  ListMinyan: any[];
  totalRecords: number;
  loader: boolean = false;
  selectRishmMinyan: RishumMinyan;
  lt: any[] = new Array;
  lp: any[] = new Array;
  ln: any[] = new Array;
  index: number;
  displayPosition: boolean;
  displayBasic: boolean = false;
  ListDays: Array<Days> = new Array<Days>();
  minyan: Minyan = new Minyan();

  constructor(private rouret: Router,
    public MinynimZminimService: MinynimZminimService,
    public PrayService: PrayService,
    public NusachService: NusachService,
    public MinyanService: MinyanService,
    public TadirutServices: TadirutService,
    public timesminyan: TimeMinyanService,
    public DateConversionService: DateConversionService,



  ) { }

  public handleAddressChange(address: Address) {

    debugger
    this.lat = address.geometry.location.lat();
    this.lng = address.geometry.location.lng();
    // this.minyan.PlaceName = address.formatted_address;

    // if(this.type=='found' )
    this.MinynimZminimService.ListFoundPlace = this.MinynimZminimService.ListMZ.map(x => Object.assign({}, x))
    for (var i = 0; i < this.MinynimZminimService.ListMZ.length; i++) {
      console.log(this.MinynimZminimService.ListMZ[i].placeX + "   " + this.MinynimZminimService.ListMZ[i].placeY);

      this.distance = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(this.lat, this.lng), new google.maps.LatLng(this.MinynimZminimService.ListMZ[i].placeX, this.MinynimZminimService.ListMZ[i].placeY));
      if (this.distance > 1500) {
        console.log(this.distance);
        console.log(this.MinynimZminimService.ListMZ[i].PlaceName);

        let index = this.MinynimZminimService.ListFoundPlace.findIndex(x => x.Id === this.MinynimZminimService.ListMZ[i].Id)
        console.log(index);

        this.MinynimZminimService.ListFoundPlace.splice(index, 1)
      }
    }
    // }
    // else {
    // this.serviceL.listLost = this.serviceL.list.map(x => Object.assign({}, x))

    // for (let i = 0; i < this.serviceL.list.length; i++) {
    //   this.distance = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(this.serviceL.list[i].lat, this.serviceL.list[i].lng), new google.maps.LatLng(this.lat, this.lng));
    //   if (this.distance > 500) {
    //     console.log(this.distance);
    //     let index = this.serviceL.listLost.findIndex(x => x.item_id === this.serviceL.list[i].item_id)
    //     this.serviceL.listLost.splice(index, 1)
    //     debugger
    //   }
  }
  ngOnInit(): void {
    this.GetAll();
    this.ListMinyan = [
      { field: 'PlaceName', header: 'מקום' },
      { field: 'Pray', header: 'תפילה' },
      { field: 'Nusach', header: 'נוסח' },
      // { field: 'Tora', header: 'ספר תורה' },
      // { field: 'Amud', header: 'סיבה לעמוד' },
      { field: 'Hour', header: 'שעה' },
      // { field: 'Before', header: ' גמישות בזמן לפני' },
      // { field: 'After', header: ' גמישות בזמן אחרי' },
      { field: 'Tadirut', header: '1תדירות' },
      { field: 'TadirutD', header: ' תדירות' },
    ];
    this.loading = true;
    this.gerAllNusach();
    this.getAllPray();
    this.getAllTadirut();
    this.ListDays.push(new Days(1, "ראשון"));
    this.ListDays.push(new Days(2, "שני"));
    this.ListDays.push(new Days(3, "שלישי"));
    this.ListDays.push(new Days(4, "רביעי"));
    this.ListDays.push(new Days(5, "חמישי"));
    this.ListDays.push(new Days(6, "שישי"));
    this.ListDays.push(new Days(7, "שבת"));
  }

  SignUp(id: number) {
    this.rouret.navigate(["RishumMinyan", id, 0])
    // this.TadirutServices.GetAll().subscribe(data => { this.TadirutServices.ListT = data }, err => { err });

  }

  getAllPray() {
    this.PrayService.GetAll().subscribe(data => {
      this.PrayService.ListP = data;
      for (this.index = 0; this.index < this.PrayService.ListP.length; this.index++) {
        debugger
        this.lp.push({ label: this.PrayService.ListP[this.index].Pray_name, value: this.PrayService.ListP[this.index].Pray_name })

      }
    }, err => { err });
  }
  gerAllNusach() {
    this.NusachService.GetAll().subscribe(data => {
      debugger
      this.NusachService.ListN = data;
      for (this.index = 0; this.index < this.NusachService.ListN.length; this.index++) {
        debugger
        this.ln.push({ label: this.NusachService.ListN[this.index].Nusach1, value: this.NusachService.ListN[this.index].Nusach1 })

      }
    }, err => { err });
  }
  dayTemplateData(date: NgbDate) {
    return {
      gregorian: (this.calendar as NgbCalendarHebrew).toGregorian(date)
    };
  }
  listDayToAllMinyan: Array<TimesMinyan> = new Array<TimesMinyan>()
  listDayToMinyan: Array<TimesMinyan> = new Array<TimesMinyan>()
  GetAll() {
    debugger
    this.MinynimZminimService.getAll().subscribe(data => {
      // this.MinynimZminimService.ListMZ = data;
      // this.loader = true; debugger; this.totalRecords = this.MinynimZminimService.ListMZ.length;
      this.MinynimZminimService.ListMZ = data; this.loader = true; this.totalRecords = this.MinynimZminimService.ListMZ.length - 2;
      this.MinynimZminimService.ListFoundPlace = this.MinynimZminimService.ListMZ.map(x => Object.assign({}, x))
      this.timesminyan.getAll().subscribe(
        d => {
          debugger
          this.listDayToAllMinyan = d
          for (let i = 0; i < this.MinynimZminimService.ListFoundPlace.length; i++) {
            if (this.MinynimZminimService.ListFoundPlace[i].Tadirut == "ימים מסוימים בשבוע") {
              this.listDayToMinyan = this.listDayToAllMinyan.filter(x => x.MinyanId == this.MinynimZminimService.ListFoundPlace[i].Id)
              this.MinynimZminimService.ListFoundPlace[i].TadirutD = " "
              this.listDayToMinyan.forEach(element => {
                this.MinynimZminimService.ListFoundPlace[i].TadirutD = this.MinynimZminimService.ListFoundPlace[i].TadirutD + (this.ListDays[element.Day].day).toString() + ", "

              });
            }
            else

              if (this.MinynimZminimService.ListFoundPlace[i].Tadirut == "חד פעמי") {
                this.DateConversionService.getHebrew(this.MinynimZminimService.ListFoundPlace[i].Datetime).subscribe(data => {
                  debugger;
                  this.MinynimZminimService.ListFoundPlace[i].heDate = data.hebrew;
                  this.MinynimZminimService.ListFoundPlace[i].TadirutD = this.MinynimZminimService.ListFoundPlace[i].heDate
                });
              }
              else
                this.MinynimZminimService.ListFoundPlace[i].TadirutD = "כל יום "


            // let h = this.getHebrew(this.MinyanService.ListFoundPlace[i].Datetime);
            // debugger;
            // this.MinyanService.ListFoundPlace[i].heDate = h;
          }
          console.log(this.MinynimZminimService.ListFoundPlace)
        },
        err => { debugger }
      )
    }, err => { });
    // this.MinyanService.getAll().subscribe(data=>{this.MinyanService.MList=data},err=>{});


  }
  loadCarsLazy(event: LazyLoadEvent) {
    this.loading = true;

    setTimeout(() => {
      if (this.MinynimZminimService.ListMZ) {
        this.MinynimZminimService.ListMZ = this.MinynimZminimService.ListMZ.slice(event.first, (event.first + event.rows));
        this.loading = false;
      }
    }, 1000);
  }
  showBasicDialog(id: number) {
    this.displayPosition = true

    debugger
    this.MinyanService.getMinyanById(id).subscribe(data => {
      debugger
      this.minyan = data;
    }, err => { });
    if (this.displayBasic == true)
      this.displayBasic = false;
    else
      this.displayBasic = true;

  }
  getAllTadirut() {
    this.TadirutServices.GetAll().subscribe(data => {
      this.TadirutServices.ListT = data;

      for (this.index = 0; this.index < this.TadirutServices.ListT.length; this.index++) {
        debugger
        this.lt.push({ label: this.TadirutServices.ListT[this.index].Description, value: this.TadirutServices.ListT[this.index].Description },
        )
      }; console.log(this.TadirutServices.ListT);
      //alert(this.TadirutServices.ListT[0].Description)
    }, err => { err });
  }
}
