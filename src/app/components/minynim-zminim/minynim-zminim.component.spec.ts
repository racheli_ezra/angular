import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinynimZminimComponent } from './minynim-zminim.component';

describe('MinynimZminimComponent', () => {
  let component: MinynimZminimComponent;
  let fixture: ComponentFixture<MinynimZminimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinynimZminimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinynimZminimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
