import { Component, OnInit, ViewChild } from '@angular/core';
import { PrayService } from './../../services/pray.service'
import { NusachService } from 'src/app/services/nusach.service';
import { ToraService } from 'src/app/services/tora.service';
import { EventService } from 'src/app/services/event.service';
import { Minyan } from 'src/app/classes/minyan';
import { MinyanService } from 'src/app/services/minyan.service';
import { findLast } from '@angular/compiler/src/directive_resolver';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { TadirutService } from 'src/app/services/tadirut.service';
import { Tadirut } from 'src/app/classes/tadirut';
import { Days } from 'src/app/classes/days';
import { TimesMinyan } from 'src/app/classes/times-minyan';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { TimeMinyanService } from 'src/app/services/time-minyan.service';
import { StatusNusachOrAmudService } from 'src/app/services/status-nusach-or-amud.service';
import { RishumMinyanComponent } from '../rishum-minyan/rishum-minyan.component';
import { RishumMinyan } from 'src/app/classes/rishum-minyan';

import {
  NgbCalendar,
  NgbCalendarHebrew, NgbDate,
  NgbDatepickerI18n,
  NgbDatepickerI18nHebrew,
  NgbDateStruct
} from '@ng-bootstrap/ng-bootstrap';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { ElementRef } from '@angular/core';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { StatusMinyanService } from 'src/app/services/status-minyan.service';
import { UserService } from 'src/app/services/user.service';
@Component({
  selector: 'app-create-minyan',
  templateUrl: './create-minyan.component.html',
  styleUrls: ['./create-minyan.component.css'],
  providers: [
    { provide: NgbCalendar, useClass: NgbCalendarHebrew },
    { provide: NgbDatepickerI18n, useClass: NgbDatepickerI18nHebrew }
  ],
})
export class CreateMinyanComponent implements OnInit {
  @ViewChild("gmap") googleMap: any;
  @ViewChild("placesRef") placesRef: GooglePlaceDirective;
  userform: FormGroup;
  // MinyanSuccessfully: boolean = false
  MinyanNotSuccessfully: boolean = false

  submitted: boolean;

  genders: SelectItem[];

  description: string;


  hour: string;
  minyan: Minyan;
  SelectTadirut: Tadirut;
  Tadirutflag: boolean;
  ListDays: Array<Days> = new Array<Days>();
  listTimesMinyan: Array<TimesMinyan> = new Array<TimesMinyan>();
  arr: string;
  idNusach: any;
  idTora: any;
  idAmud: any;
  idPray: any;
  idTadirut: Tadirut = new Tadirut();
  flag1: boolean = false;
  flag2: boolean = false;
  flag3: boolean = false;
  rishumMinyan: RishumMinyan;
  model: NgbDateStruct;
  EvriDatetime: string;
  userLogin1: boolean;
  // מערך ששומר אוביקט מסוג מנין ומערך עם כל הימים שהמנין יתקיים כדי להעביר להוספה 
  MinyanDinamic: any[] = new Array<any>();
  constructor(
    private fb: FormBuilder, //private messageService: MessageService,

    public PrayService: PrayService,
    public NusachService: NusachService,
    public ToraService: ToraService,
    public EventService: EventService,
    public MinyanService: MinyanService,
    public TadirutServices: TadirutService,
    public TimeMinyanService: TimeMinyanService,
    public StatusMinyanService: StatusMinyanService,
    public StatusNusachOrAmudService: StatusNusachOrAmudService,
    public UserService: UserService,
    private calendar: NgbCalendar, public i18n: NgbDatepickerI18n,
    private route: Router) {
    this.minyan = new Minyan();
    // this.minyan.PlaceName = ' ';
    // (0, 0, "", 0, "", 0, "", 0, "", 0, ""," { hours: 0, minutes: 0 }", 1, 0, new Date(), 0, "");
    this.rishumMinyan = new RishumMinyan();
    this.idTadirut.Id = null;
    this.dayTemplateData = this.dayTemplateData.bind(this);

  }

  ngOnInit() {
    this.getAllTadirut();
    this.getAllPray();
    this.gerAllNusach();
    this.gerAllTora();
    this.getAllEvent();
    this.getAllStatusNusachOrAmud();
    this.userLogin();
    this.NusachService.GetIdByNusach("לא משנה").subscribe(data => { this.idNusach = this.NusachService.ListN.find(s => s.Id == data.Id) }, err => { alert("gvcvbckhc") })
    this.ToraService.GetIdByTora("לא משנה").subscribe(data => { this.idTora = this.ToraService.ListT.find(t => t.Id == data.Id) });
    this.EventService.GetIdByEvent("אין צורך").subscribe(data => { this.idAmud = this.EventService.ListRE.find(a => a.Id == data.Id) });
    // this.TadirutService.get("חד פעמי").subscribe(data => { this. =this.EventService.ListRE.find(a=>a.Id== data.Id )});

    this.ListDays.push(new Days(1, "ראשון"));
    this.ListDays.push(new Days(2, "שני"));
    this.ListDays.push(new Days(3, "שלישי"));
    this.ListDays.push(new Days(4, "רביעי"));
    this.ListDays.push(new Days(5, "חמישי"));
    this.ListDays.push(new Days(6, "שישי"));
    this.ListDays.push(new Days(7, "שבת"));


  }
  @ViewChild('places') places: GooglePlaceDirective;
  @ViewChild('search') public searchElement: ElementRef;

  public handleAddressChange(address: Address) {
    debugger
    this.minyan.placeX = address.geometry.location.lat();
    this.minyan.placeY = address.geometry.location.lng();
    this.minyan.PlaceName = address.formatted_address;
  }


  getAllTadirut() {
    this.TadirutServices.GetAll().subscribe(data => {
      this.TadirutServices.ListT = data; console.log(this.TadirutServices.ListT);
      //alert(this.TadirutServices.ListT[0].Description)
    }, err => { err });

  }

  getAllPray() {
    this.PrayService.GetAll().subscribe(data => { this.PrayService.ListP = data }, err => { err });
  }
  gerAllNusach() {
    this.NusachService.GetAll().subscribe(data => { this.NusachService.ListN = data }, err => { err });
  }
  gerAllTora() {
    this.ToraService.GetAll().subscribe(data => { this.ToraService.ListT = data }, err => { err })
  }
  getAllEvent() {
    this.EventService.GetAll().subscribe(data => { this.EventService.ListRE = data }, err => { err });
  }



  userLogin() {
    debugger;
    if (this.UserService.user.id == null) {
      this.userLogin1 = false;
    }
    else {
      this.userLogin1 = true;
    }
  }
  User() {
    this.route.navigate(["User/", 2])

  }
  dayTemplateData(date: NgbDate) {
    return {
      gregorian: (this.calendar as NgbCalendarHebrew).toGregorian(date)
    };
  }

  selectToday() {
    this.model = this.calendar.getToday();
  }
  h: string;
  addMinyan() {
    debugger;
    this.StatusMinyanService.GetIdByStarusMinyan("לא פעיל").subscribe(data => {
      this.minyan.situationId = data.valueOf()
      this.minyan.TadirutId = this.idTadirut.Id;
      this.minyan.NusachId = this.idNusach.Id;
      this.minyan.PrayId = this.idPray.Id;
      this.minyan.ToraId = this.idTora.Id;
      this.minyan.reasonAmudId = this.idAmud.Id;
      this.MinyanDinamic[0] = this.minyan;
      debugger;

      // this.h = this.minyan.Hour.toString();
      this.MinyanDinamic[1] = this.minyan.Hour.toUTCString();
      this.MinyanDinamic[2] = this.listTimesMinyan;
      this.MinyanDinamic[3] = this.EvriDatetime;
      console.log(this.minyan.Hour);
      this.MinyanService.Add(this.MinyanDinamic).subscribe(data => {
        this.minyan = data; this.MinyanService.MList.push(this.minyan);
        // if (this.listTimesMinyan != null) {
        //   debugger
        //   for(let i=0;i<this.listTimesMinyan.length;i++)
        //   {
        //     this.listTimesMinyan[i].MinyanId=this.minyan.Id;
        //   }
        //   this.TimeMinyanService.add(this.listTimesMinyan).subscribe(Date=>{},err=>{});

        // } 
        debugger;
        this.route.navigate(["RishumMinyan/" + this.minyan.Id + '/' + 0])


      }, err => {
        debugger; this.MinyanNotSuccessfully = true;
      });
    }, err => {
    });

  }
  chekeTadirut() {
    // debugger;
    var ind = this.TadirutServices.ListT.findIndex(t => t.Id == this.idTadirut.Id);
    if (ind != -1) {
      if (this.TadirutServices.ListT[ind].Description == ("ימים מסוימים בשבוע"))
        return true;
    }
    return false
  }
  disabled() {
    var ind = this.TadirutServices.ListT.findIndex(t => t.Id == this.idTadirut.Id);
    if (ind != -1) {
      if (this.TadirutServices.ListT[ind].Description == ("חד פעמי"))
        return true;
    }
    return false
  }
  addORDelDay(event: Event, day: Days) {
    debugger;
    if (event.currentTarget)
      this.listTimesMinyan.push(new TimesMinyan(0, 0, day.id));
    else {
      var i = this.listTimesMinyan.findIndex(d => d.Day == day.id)
      this.listTimesMinyan.splice(i, 1);
    }

  }
  // onSubmit(value: string) {
  //   this.submitted = true;
  //   this.messageService.add({ severity: 'info', summary: 'Success', detail: 'Form Submitted' });
  // }
  // get diagnostic() { return JSON.stringify(this.userform.value); }

  getAllStatusNusachOrAmud() {
    this.StatusNusachOrAmudService.GetAll().subscribe(data => {
      this.StatusNusachOrAmudService.listS = data;
      this.rishumMinyan.StatusAmud = this.StatusNusachOrAmudService.listS.find(a => a.Status == ("חובה")).Id;
      this.rishumMinyan.StatusNusach = this.StatusNusachOrAmudService.listS.find(a => a.Status == ("חובה")).Id;
      debugger
      this.rishumMinyan.statusTora = this.StatusNusachOrAmudService.listS.find(a => a.Status == ("חובה")).Id;
    }, err => { })
  }
  selectedNusach() {
    if (this.idNusach.Nusach1 == "לא משנה")
      this.flag1 = false;
    else
      this.flag1 = true;
  }
  selectedAmud() {
    if (this.idAmud.Reson == "אין צורך")
      this.flag2 = false;
    else
      this.flag2 = true;
  }
  selectedTora() {
    if (this.idTora.Nusach == "לא משנה")
      this.flag3 = false;
    else
      this.flag3 = true;
    ///////
  }
}
