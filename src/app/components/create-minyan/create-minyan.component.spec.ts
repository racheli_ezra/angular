import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMinyanComponent } from './create-minyan.component';

describe('CreateMinyanComponent', () => {
  let component: CreateMinyanComponent;
  let fixture: ComponentFixture<CreateMinyanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateMinyanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMinyanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
