import { Component } from '@angular/core';
import { MegaMenuItem } from 'primeng/api';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  "styles": [
    "node_modules/primeflex/primeflex.css"
  ],
})
export class AppComponent {
  items: MegaMenuItem[];
  MenuItem: MenuItem[];

  lat = 51.678418;
  lng = 7.809007;
  image = "../assets/logo1/ruth_hubara_logo_minyan.jpg"
  constructor(protected route: Router) { }
  ngOnInit() {
    this.items = [
      {
        label: 'דף הבית', icon: 'pi pi-home', routerLink: "home"
      },
      {
        label: 'רשימת מנינים', icon: 'pi pi-list', routerLink: "MinyanDetailes"
      },
      {
        label: 'צור מנין', icon: 'pi pi-fw pi-users', routerLink: "CreateMinyan"
      },
      {
        label: 'רשימת מנינים זמינים', icon: 'pi pi-list', routerLink: "MinyaninZminim",
      },
      {
        label: 'כניסה', icon: 'pi pi-user ', routerLink: "User/" + 1
      },
      {
        label: 'המנינים שלי', icon: 'pi pi-user ', routerLink: "MyMinyan"
      }
      ,

    ]
    this.MenuItem = [
      {
        label: 'דף הבית', icon: 'pi pi-home', routerLink: "home", style: 'width:50px'
      },
      {
        label: 'רשימת מנינים', icon: 'pi pi-list', routerLink: "MinyanDetailes"
      },
      {
        label: 'צור מנין', icon: 'pi pi-fw pi-users', routerLink: "CreateMinyan"
      },
      {
        label: 'רשימת מנינים זמינים', icon: 'pi pi-list', routerLink: "MinyaninZminim",
      },
      {
        label: 'כניסה', icon: 'pi pi-user ', routerLink: "User/" + 1
      },
      {
        label: 'המנינים שלי', icon: 'pi pi-user ', routerLink: "MyMinyan"
      }
      ,
    ];
  }
}

