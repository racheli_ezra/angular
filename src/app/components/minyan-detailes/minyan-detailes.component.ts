import { Component, OnInit } from '@angular/core';
import { MinyanService } from 'src/app/services/minyan.service';
// import { Observable } from 'rxjs';
import { DateConversionService } from 'src/app/services/date-conversion.service';

import { PrayService } from 'src/app/services/pray.service';
import { NusachService } from 'src/app/services/nusach.service';
import { ToraService } from 'src/app/services/tora.service';
import { EventService } from 'src/app/services/event.service';
import { Router } from '@angular/router';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { ViewChild, ElementRef } from '@angular/core';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { LazyLoadEvent } from 'primeng/api';
import { FilterUtils } from 'primeng/utils';
// import { transition, trigger, state, style, animate } from '@angular/animations';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { RishumMinyan } from 'src/app/classes/rishum-minyan';
import { TadirutService } from 'src/app/services/tadirut.service'
import { Minyan } from 'src/app/classes/minyan';
import { NgbCalendar, NgbCalendarHebrew, NgbDate, NgbDatepickerI18n, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { TimesMinyan } from 'src/app/classes/times-minyan';
import { TimeMinyanService } from 'src/app/services/time-minyan.service';
import { Days } from 'src/app/classes/days';
// import { LazyLoadEvent } from 'primeng/api';
// import { Address } from 'ngx-google-places-autocomplete/objects/address';
// import { Directive, ElementRef, EventEmitter, OnInit, Output } from '@angular/core';
declare const google: any;

@Component({
  selector: 'app-minyan-detailes',
  templateUrl: './minyan-detailes.component.html',
  // styleUrls: ['./minyan-detailes.component.css']
  animations: [
    trigger('rowExpansionTrigger', [
      state('void', style({
        transform: 'translateX(-10%)',
        opacity: 0
      })),
      state('active', style({
        transform: 'translateX(0)',
        opacity: 1
      })),
      transition('* <=> *', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
    ])
  ]
})
export class MinyanDetailesComponent implements OnInit {
  [x: string]: any;
  @ViewChild("gmap") googleMap: any;
  @ViewChild("placesRef") placesRef: GooglePlaceDirective;
  // @Directive({ selector: '[googleplaces]' })
  // @Directive({
  //   selector: '[Googleplace]',
  //   providers: [NgModel],
  //   host: {
  //     '(input)' : 'onInputChange()'
  //   }
  // })

  displayPosition: boolean;
  displayBasic: boolean = false;
  lat: number;
  lng: number;
  totalRecords: number;
  loading: boolean;
  ListMinyan: any[];
  lt: any[] = new Array;
  lp: any[] = new Array;
  ln: any[] = new Array;
  index: number;
  type: string
  distance: number
  model: NgbDateStruct;
  EvriDatetime: string;
  ListDays: Array<Days> = new Array<Days>();
  loader: boolean = false;
  selectedCar1: RishumMinyan;
  minyan: Minyan = new Minyan();
  constructor(private ref: ElementRef,
    public MinyanService: MinyanService,
    public PrayService: PrayService,
    public TadirutServices: TadirutService,
    public NusachService: NusachService,
    public ToraServicet: ToraService,
    public EventService: EventService,
    public rouret: Router,
    private calendar: NgbCalendar,
    public i18n: NgbDatepickerI18n,
    public DateConversionService: DateConversionService,
    public timesminyan: TimeMinyanService

  ) { }
  @ViewChild('places') places: GooglePlaceDirective;
  @ViewChild('search') public searchElement: ElementRef;

  public handleAddressChange(address: Address) {
    this.lat = address.geometry.location.lat();
    this.lng = address.geometry.location.lng();
    // this.minyan.PlaceName = address.formatted_address;

    // if(this.type=='found' )
    this.MinyanService.ListFoundPlace = this.MinyanService.MList.map(x => Object.assign({}, x))
    for (var i = 0; i < this.MinyanService.MList.length; i++) {
      console.log(this.MinyanService.MList[i].placeX + "   " + this.MinyanService.MList[i].placeY);

      this.distance = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(this.lat, this.lng), new google.maps.LatLng(this.MinyanService.MList[i].placeX, this.MinyanService.MList[i].placeY));
      if (this.distance > 1500) {
        console.log(this.distance);
        console.log(this.MinyanService.MList[i].PlaceName);

        let index = this.MinyanService.ListFoundPlace.findIndex(x => x.Id === this.MinyanService.MList[i].Id)
        console.log(index);

        this.MinyanService.ListFoundPlace.splice(index, 1)
      }
    }
    // }
    // else {
    // this.serviceL.listLost = this.serviceL.list.map(x => Object.assign({}, x))

    // for (let i = 0; i < this.serviceL.list.length; i++) {
    //   this.distance = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(this.serviceL.list[i].lat, this.serviceL.list[i].lng), new google.maps.LatLng(this.lat, this.lng));
    //   if (this.distance > 500) {
    //     console.log(this.distance);
    //     let index = this.serviceL.listLost.findIndex(x => x.item_id === this.serviceL.list[i].item_id)
    //     this.serviceL.listLost.splice(index, 1)
    //     debugger
    //   }
  }






  ngOnInit() {

    this.getAllTadirut();
    this.getAllPray();
    this.gerAllNusach();
    this.gerAllTora();
    this.getAllEvent();


    this.GetAll();
    // this.MinyanService.getCarsSmall().then(cars => this.ListMinyan = cars);

    this.ListDays.push(new Days(1, "ראשון"));
    this.ListDays.push(new Days(2, "שני"));
    this.ListDays.push(new Days(3, "שלישי"));
    this.ListDays.push(new Days(4, "רביעי"));
    this.ListDays.push(new Days(5, "חמישי"));
    this.ListDays.push(new Days(6, "שישי"));
    this.ListDays.push(new Days(7, "שבת"));

    this.ListMinyan = [
      { field: 'PlaceName', header: 'מקום' },
      { field: 'Pray', header: 'תפילה' },
      { field: 'Nusach', header: 'נוסח' },
      // { field: 'Tora', header: 'ספר תורה' },
      // { field: 'Amud', header: 'סיבה לעמוד' },
      { field: 'Hour', header: 'שעה' },
      // { field: 'Before', header: ' גמישות בזמן לפני' },
      // { field: 'After', header: ' גמישות בזמן אחרי' },
      { field: 'Tadirut', header: '1תדירות' },
      { field: 'TadirutD', header: ' תדירות' },
    ];
    this.loading = true;
    FilterUtils['custom'] = (value, filter): boolean => {
      if (filter === undefined || filter === null || filter.trim() === '') {
        return true;
      }

      if (value === undefined || value === null) {
        return false;
      }

      return parseInt(filter) > value;
    }
  }
  getAllTadirut() {
    this.TadirutServices.GetAll().subscribe(data => {
      this.TadirutServices.ListT = data;

      for (this.index = 0; this.index < this.TadirutServices.ListT.length; this.index++) {
        this.lt.push({ label: this.TadirutServices.ListT[this.index].Description, value: this.TadirutServices.ListT[this.index].Description },
        )
      }; console.log(this.TadirutServices.ListT);
      //alert(this.TadirutServices.ListT[0].Description)
    }, err => { err });
  }

  getAllPray() {
    this.PrayService.GetAll().subscribe(data => {
      this.PrayService.ListP = data;
      for (this.index = 0; this.index < this.PrayService.ListP.length; this.index++) {
        this.lp.push({ label: this.PrayService.ListP[this.index].Pray_name, value: this.PrayService.ListP[this.index].Pray_name })

      }
    }, err => { err });
  }
  gerAllNusach() {
    this.NusachService.GetAll().subscribe(data => {
      this.NusachService.ListN = data;
      for (this.index = 0; this.index < this.NusachService.ListN.length; this.index++) {

        this.ln.push({ label: this.NusachService.ListN[this.index].Nusach1, value: this.NusachService.ListN[this.index].Nusach1 })

      }
    }, err => { err });
  }
  gerAllTora() {
    this.ToraServicet.GetAll().subscribe(data => { this.ToraServicet.ListT = data }, err => { err })
  }
  getAllEvent() {
    this.EventService.GetAll().subscribe(data => { this.EventService.ListRE = data }, err => { err });
  }

  // onMapReady1(map, lat: number, lng: number) {
  //   debugger
  //   var position = new google.maps.LatLng(lat, lng);
  //   map.setCenter(position);
  // }
  // public handleAddressChange(address: Address) {
  //   var m = address.geometry.location;
  //   this.lat = address.geometry.location.lat();
  //   this.lng = address.geometry.location.lng();


  // }
  dayTemplateData(date: NgbDate) {
    return {
      gregorian: (this.calendar as NgbCalendarHebrew).toGregorian(date)
    };
  }
  listDayToAllMinyan: Array<TimesMinyan> = new Array<TimesMinyan>()
  listDayToMinyan: Array<TimesMinyan> = new Array<TimesMinyan>()
  GetAll() {
    debugger
    this.MinyanService.getAll().subscribe(data => {
      this.MinyanService.MList = data; this.loader = true; this.totalRecords = this.MinyanService.MList.length - 2;
      this.MinyanService.ListFoundPlace = this.MinyanService.MList.map(x => Object.assign({}, x))
      this.timesminyan.getAll().subscribe(
        d => {
          debugger
          this.listDayToAllMinyan = d
          for (let i = 0; i < this.MinyanService.ListFoundPlace.length; i++) {
            if (this.MinyanService.ListFoundPlace[i].Tadirut == "ימים מסוימים בשבוע") {
              this.listDayToMinyan = this.listDayToAllMinyan.filter(x => x.MinyanId == this.MinyanService.ListFoundPlace[i].Id)
              this.MinyanService.ListFoundPlace[i].TadirutD = " "
              this.listDayToMinyan.forEach(element => {
                debugger;
                this.MinyanService.ListFoundPlace[i].TadirutD = this.MinyanService.ListFoundPlace[i].TadirutD +(this.ListDays[element.Day-1].day ).toString()+ ", "

              });
            }
            else

              if (this.MinyanService.ListFoundPlace[i].Tadirut == "חד פעמי") {
                this.DateConversionService.getHebrew(this.MinyanService.ListFoundPlace[i].Datetime).subscribe(data => {
                  debugger;
                  this.MinyanService.ListFoundPlace[i].heDate = data.hebrew;
                  this.MinyanService.ListFoundPlace[i].TadirutD = this.MinyanService.ListFoundPlace[i].heDate
                });
              }
              else
                this.MinyanService.ListFoundPlace[i].TadirutD = "כל יום "


            // let h = this.getHebrew(this.MinyanService.ListFoundPlace[i].Datetime);
            // debugger;
            // this.MinyanService.ListFoundPlace[i].heDate = h;
          }
          console.log(this.MinyanService.ListFoundPlace)
        },
        err => { debugger }
      )
    }, err => { debugger });
    //datasource imitation
  }

  //הרשמה למנין
  SignUp(id: number) {

    this.rouret.navigate(["RishumMinyan/" + id + '/' + 1])

  }

  loadCarsLazy(event: LazyLoadEvent) {
    this.loading = true;

    setTimeout(() => {
      if (this.MinyanService.MList) {
        this.MinyanService.MList = this.MinyanService.MList.slice(event.first, (event.first + event.rows));
        this.loading = false;
      }
    }, 1000);
  }
  showBasicDialog(id: number) {
    // this.displayPosition = true

    this.MinyanService.getMinyanById(id).subscribe(data => {
      this.minyan = data;
      this.displayBasic = !this.displayBasic;
    }, err => { });

  }

  // onDateSelect(value) {
  //   this.td.filter(this.formatDate(value), 'date', 'equals');
  // }

  // formatDate(date) {
  //   let month = date.getMonth() + 1;
  //   let day = date.getDate();

  //   if (month < 10) {
  //     month = '0' + month;
  //   }

  //   if (day < 10) {
  //     day = '0' + day;
  //   }

  //   return date.getFullYear() + '-' + month + '-' + day;
  // }

  // getHebrew(date: Date) {

  //   let hebrewDate =  " ";
  //   this.DateConversionService.getHebrew(date).subscribe(data => {
  //     hebrewDate = data.hebrew;
  //     });
  //   return hebrewDate;

  // }

}
