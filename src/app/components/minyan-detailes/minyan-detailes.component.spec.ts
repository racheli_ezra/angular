import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinyanDetailesComponent } from './minyan-detailes.component';

describe('MinyanDetailesComponent', () => {
  let component: MinyanDetailesComponent;
  let fixture: ComponentFixture<MinyanDetailesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinyanDetailesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinyanDetailesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
