import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyMinynimComponent } from './my-minynim.component';

describe('MyMinynimComponent', () => {
  let component: MyMinynimComponent;
  let fixture: ComponentFixture<MyMinynimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyMinynimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyMinynimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
