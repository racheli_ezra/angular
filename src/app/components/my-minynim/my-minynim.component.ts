import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/classes/user';
import { MyMinyanService } from 'src/app/services/my-minyan.service';
// import { MessageService } from 'primeng/api';
import { PrayService } from 'src/app/services/pray.service';
import { NusachService } from 'src/app/services/nusach.service';
import { EventService } from 'src/app/services/event.service';
// import { AnyARecord } from 'dns';
import { ToraService } from 'src/app/services/tora.service';
import { TadirutService } from 'src/app/services/tadirut.service';
import { Minyan } from 'src/app/classes/minyan';
import { UserService } from 'src/app/services/user.service';
import { RishumMinyanService } from '../../services/rishum-minyan.service'
import { from } from 'rxjs';
import { StatusMinyanService } from 'src/app/services/status-minyan.service';
import { MinyanService } from 'src/app/services/minyan.service';
import { TimeMinyanService } from 'src/app/services/time-minyan.service';
// import { RishumMinyan } from 'src/app/services/rishum-minyan.service'
// import { MessageService } from 'primeng/api/messageservice';
import { DateConversionService } from 'src/app/services/date-conversion.service';
import { TimesMinyan } from 'src/app/classes/times-minyan';

@Component({
  selector: 'app-my-minynim',
  templateUrl: './my-minynim.component.html',
  styleUrls: ['./my-minynim.component.css']
})
export class MyMinynimComponent implements OnInit {

  user: User
  PrayId: any
  NusachId: any
  AmudID: any
  ToraId: any
  Tadirut: any
  delated: boolean = false;
  notDeleted: boolean = true;
  minyanIdToDelete: number
  delete: boolean
  statueMinyan: string
  situationId: number
  constructor(public MyMinyanService: MyMinyanService,
    // private messageService: MessageService
    public PrayService: PrayService,
    public NusachService: NusachService,
    public EventService: EventService,
    public ToraService: ToraService,
    public TadirutService: TadirutService,
    public UserService: UserService,
    public RishumMinyanService: RishumMinyanService,
    public StatusMinyanService: StatusMinyanService,
    public DateConversionService: DateConversionService,
    public timesminyan: TimeMinyanService) { }

  ngOnInit() {
    this.PrayService.GetAll().subscribe(data => this.PrayService.ListP = data);
    this.NusachService.GetAll().subscribe(data => this.NusachService.ListN = data);
    this.EventService.GetAll().subscribe(data => this.EventService.ListRE = data);
    this.ToraService.GetAll().subscribe(data => this.ToraService.ListT = data);
    this.TadirutService.GetAll().subscribe(data => this.TadirutService.ListT = data);
    this.GetAllMinyanimByUserId(this.UserService.user.id);

  }
  // GetStatusMinyanById(id: number) {
  //   debugger
  //   this.StatusMinyanService.GetStatusMinyanById(id).subscribe(data => { this.statueMinyan = data }, err => { })
  // }
  listDayToAllMinyan: Array<TimesMinyan> = new Array<TimesMinyan>()
  listDayToMinyan: Array<TimesMinyan> = new Array<TimesMinyan>()

  GetAllMinyanimByUserId(id: string) {
    this.MyMinyanService.GetAllMinyanimByUserId(id).subscribe(data => { debugger; this.MyMinyanService.ListMM = data 
    for(let i=0;i<this.MyMinyanService.ListMM.length;i++)
    {
      this.StatusMinyanService.GetStatusMinyanById(this.MyMinyanService.ListMM[i].situationId).subscribe(data =>
         { this.MyMinyanService.ListMM[i].situation = data}, err => { })
         if (this.MyMinyanService.ListMM[i].Tadirut == "ימים מסוימים בשבוע") {
          this.listDayToMinyan = this.listDayToAllMinyan.filter(x => x.MinyanId == this.MyMinyanService.ListMM[i].Id)
          this.MyMinyanService.ListMM[i].TadirutD = " "
          this.listDayToMinyan.forEach(element => {
            this.MyMinyanService.ListMM[i].TadirutD = this.MyMinyanService.ListMM[i].TadirutD + element.Day + " "
  
          });
        }
        else
  
          if (this.MyMinyanService.ListMM[i].Tadirut == "חד פעמי") {
            this.DateConversionService.getHebrew(this.MyMinyanService.ListMM[i].Datetime).subscribe(data => {
              debugger;
              this.MyMinyanService.ListMM[i].heDate = data.hebrew;
              this.MyMinyanService.ListMM[i].TadirutD = this.MyMinyanService.ListMM[i].heDate
            });
          }
          else
            this.MyMinyanService.ListMM[i].TadirutD = "כל יום "
  
  
    }
    


}, err => { debugger });
    debugger

  }

  onRowEditInit(minyan: Minyan) {


    // this.clonedCars[car.vin] = {...car};
    // this.MyMinyanService.
  }

  onRowEditSave(minyan: Minyan) {
    // if (car.year > 0) {
    //     delete this.clonedCars[car.vin];
    //     this.messageService.add({severity:'success', summary: 'Success', detail:'Car is updated'});
    // }
    // else {
    //     this.messageService.add({severity:'error', summary: 'Error', detail:'Year is required'});
    // }
  }
  showModalDialog(MinyanId: number) {

    this.minyanIdToDelete = MinyanId;
    this.delete = true;

  }

  onRowEditCancel() {
    debugger
    this.RishumMinyanService.cancalRishum(this.minyanIdToDelete, this.UserService.user.id).subscribe(data => { debugger; this.notDeleted = data, this.delated = data, this.GetAllMinyanimByUserId(this.UserService.user.id), this.delete = false }, err => { })
    // this.cars2[index] = this.clonedCars[car.vin];
    // delete this.clonedCars[car.vin];
  }
}




