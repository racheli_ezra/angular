import { Component, OnInit } from '@angular/core';
// import {
//   NgbCalendar,
//   NgbCalendarHebrew, NgbDate,
//   NgbDatepickerI18n,
//   NgbDatepickerI18nHebrew,
//   NgbDateStruct
// } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from 'src/app/services/user.service';
@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {

  IsShow:boolean;
  newPassword:boolean;
  checkUser: boolean
  failed:boolean;
  obj: { mail: string,  phone: string }

  // loginText = "../../assets/logos/password1.png"

  EMAIL_REGEX = '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$'

  constructor( private UserService:UserService ) { }

  ngOnInit(): void {
    this.obj = { phone: "", mail: "" }
    this.checkUser = true;
    this.newPassword = false
    this.failed = false
  }

  sendNewPassword() {
    debugger
    this.UserService.sendNewPassword(this.obj).subscribe(data => {
      debugger
      if (data) {
        debugger
        this.checkUser = false;
        this.failed = false;
        this.newPassword = true;
      }
      else {
        this.checkUser = false;
        this.failed = true;
        this.newPassword = false;
      }
    })
  }

  //?????????????????????????????
  tryAgain(){
    this.obj = { phone: "", mail: "" }

    this.checkUser = true;
        this.failed = false;
        this.newPassword = false;
  }

  ShowPass() {
    debugger
    this.IsShow = !this.IsShow
    let d = document.getElementsByName("Password")[0]

    if (d)
      d.setAttribute("type", "text")
  }
  CoverPass() {
    debugger
    this.IsShow = !this.IsShow
    let d = document.getElementsByName("Password")[0]
    if (d)
      d.setAttribute("type", "password")
  }

}
