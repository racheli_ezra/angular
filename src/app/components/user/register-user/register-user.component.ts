import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { User } from '../../../classes/user';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {

  newUser = new User();
  register: boolean = false;
  mail: boolean = false;
  usersMail: Array<String>;

  IsShow: boolean;
  // newPassword: boolean
  NewPassword1: string = ""
  NewPassword2: string = ""

  EMAIL_REGEX = '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$'

  constructor(private UserService: UserService) { }

  ngOnInit(): void {
    // this.getAllUsersMail();
  }

  registerUser() {
    this.UserService.add(this.newUser).subscribe(data => { this.register = true }, err => { })
  }
  
  tryAgain() {
    this.newUser = new User();
  }

  getAllUsersMail() {
    this.UserService.GetAllUsersMail().subscribe(data => { debugger; this.usersMail = data })
  }

  sameMail() {
    debugger;
    if (this.usersMail.filter(u => u == this.newUser.id))
      this.mail = true;
  }

  ShowPass() {
    debugger
    this.IsShow = !this.IsShow
    let d = document.getElementsByName("Password")[0]

    if (d)
      d.setAttribute("type", "text")
  }
  CoverPass() {
    debugger
    this.IsShow = !this.IsShow
    let d = document.getElementsByName("Password")[0]
    if (d)
      d.setAttribute("type", "password")
  }

  isSamePass() {
    if (this.NewPassword1 === this.NewPassword2)
      return true;
    return false
  }
}
