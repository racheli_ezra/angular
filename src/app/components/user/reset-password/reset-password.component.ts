import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/classes/user';
import { UserService } from 'src/app/services/user.service';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  IsShow: boolean;
  IsShow2: boolean;
  UserMail: string;
  UserPassword: string;
  saving: boolean;
  user: User;
  success: boolean = false;
  saving1: boolean = false;

  checkUser: boolean
  failed: boolean
  newPassword: boolean
  NewPassword1: string = ""
  NewPassword2: string = ""

  EMAIL_REGEX = '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$'


  constructor(public UserService: UserService) {

  }

  ngOnInit(): void {

    debugger
    this.UserMail = "";
    this.UserPassword = "";
    this.saving = false;
    this.checkUser = true;
    this.newPassword = false
    this.failed = false
  }
  ValidateUser() {
    this.saving = true;
    // לשאול את אסתי שטראוס מה זה
    delay(500000);
    debugger;
    this.UserService.getUser(this.UserMail, this.UserPassword).subscribe(data => {
      this.saving = false;
      this.checkUser = false;
      debugger;
      if (data != null) {
        debugger;
        this.user = data;
        this.newPassword = true;
      }
      else {
        debugger;
        this.failed = true;
      }
    },
      err => { console.log(err) }
    )
  }

  tryAgain() {
    this.UserMail = "";
    this.UserPassword = ""
    this.checkUser = true;
    this.failed = false;
    this.newPassword = false;
  }
  ShowPass(p: string,) {
    debugger
    if (p != "NewPass2")
      this.IsShow = !this.IsShow
    else
      this.IsShow2 = !this.IsShow2
    // let d = document.getElementsByName("Password")[0]
    let d = document.getElementsByName(p)[0]

    if (d)
      d.setAttribute("type", "text")
  }
  CoverPass(p: string) {
    debugger
    if (p != "NewPass2")
      this.IsShow = !this.IsShow
    else
      this.IsShow2 = !this.IsShow2
    // let d = document.getElementsByName("Password")[0]
    let d = document.getElementsByName(p)[0]
    if (d) {
      d.setAttribute("type", "password")
    }
  }

  isSamePass() {
    if (this.NewPassword1 === this.NewPassword2)
      return true;
    return false
  }

  ChangePassword() {
    this.saving1 = true;
    this.UserService.changePassword(this.user.id, this.NewPassword1).subscribe(data => {
      this.saving1 = false
      this.newPassword = false;
      this.success = true;
    })
  }
}
