import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/classes/user';
import { UserService } from 'src/app/services/user.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { RishumMinyanService } from 'src/app/services/rishum-minyan.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers: [MessageService],
})
export class UserComponent implements OnInit {
  IsShow: boolean;
  IsFailed: boolean;
  user: User = new User();
  userform: FormGroup;
  nituv: number;
  EMAIL_REGEX = '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$'

  constructor(private route: Router,
    public UserService: UserService,
    public active: ActivatedRoute,
    public RishumMinyanService: RishumMinyanService,
    private fb: FormBuilder, private messageService: MessageService,

  ) { }

  ngOnInit() {
    this.active.params.subscribe(f => {
      debugger
      //הנתונים לא באים טוב!!!!
      this.nituv = f["nituv"]
    })
    this.IsFailed = this.IsShow = false
    this.userform = this.fb.group({
    });
  }

  failed() {
    this.IsFailed = true
    // this.user.id = ""
    // this.user.password = ""
    this.route.navigate(["AddUser/" + this.user.id + "/" + this.user.password])

  }

  ShowPassword() {

    this.IsShow = !this.IsShow;
    let d = document.getElementsByName("Password")[0]

    if (d)
      d.setAttribute("type", "text")
  }

  CoverPassword() {

    this.IsShow = !this.IsShow;
    let d = document.getElementsByName("Password")[0]

    if (d)
      d.setAttribute("type", "password")
  }

  LogIn() {
    debugger
    this.UserService.getUser(this.user.id, this.user.password).subscribe(data => {
      this.UserService.user = data;
      debugger;
      if (this.UserService.user.id != null) {
        if (this.nituv == 1)
          this.route.navigate(["MyMinyan"])
        else
          if (this.nituv == 2)
            this.route.navigate(["CreateMinyan"])
        if (this.nituv == 3) {
          debugger
          this.route.navigate(["RishumMinyan/" + this.RishumMinyanService.id + "/" + this.RishumMinyanService.isManager])

        }


      }
      else {
        debugger;
        this.failed();
      }
    },
      err => { })

    // this.route.navigate(["AddUser/"+this.user.Id+'/'+this.user.password]);
  }
  
}
