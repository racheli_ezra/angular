import { DatepickerServiceInputs } from '@ng-bootstrap/ng-bootstrap/datepicker/datepicker-service';

export class Minyan {
    constructor(public Id?: number,
        public placeX?: number,
        public placeY?: number,
        public PlaceName: string = "",
        public PrayId?: number,
        public Pray?: string,
        public NusachId?: number,
        public Nusach?: string,
        public ToraId?: number,
        public Tora?: string,
        public reasonAmudId?: number,
        public Amud?: string,
        public Hour?: Date,
        public Before?: number,
        public After?: number,
        public Datetime: Date = new Date(),
        public TadirutId?: number,
        public Tadirut?: string,
        public TadirutD?: string,
        public situation?: string,
        public situationId?: number,
        public KadishId?: number,
        public Kadish?: string,
        public heDate?: string,
        // public EvriDatetime?: string
    ) { }
}














// StatusMinyan = null


