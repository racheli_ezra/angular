export class RishumMinyan {

    constructor(
        public Id?: number,
        public MinyanId?: number,
        public Name?: string,
        public Mail?: string,
        public IsManger?: boolean,
        public NusachId?: number,
        public Nusach?: number,
        public Amud?: string,
        public ReasonAmudId?: number,
        public Kadish?: string,
        public Arrived?: boolean,
        public Hour?: Date,
        public GmishutInHourBefore?: number,
        public Eshur?: boolean,
        public KadishId?: number,
        public StatusAmud?: number,
        public StatusKadish?: number,
        public StatusNusach?: number,
        public priority?: number,
        public GmishutInHourAfter?: number,
        public torahId?:number,
        public statusTora?:number 
    ) { }
}
